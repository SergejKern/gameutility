﻿using System;
using UnityEngine;

namespace GameUtility.Data.Feedback
{
    public interface IRumble
    {
        void SetRumble(float duration);
        void SetRumble(float duration, float factor);
        void SetRumble(RumbleData data);
    }

    [Serializable]
    public struct RumbleData
    {
        public AnimationCurve Speed;
        public float Factor;
        public float Duration;
        [HideInInspector]
        public float Time;
    }
}