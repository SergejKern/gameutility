﻿using System.Collections.Generic;
using Core.Extensions;
using Core.Unity.Extensions;
using UnityEngine;

namespace GameUtility.Data.Visual
{
    [RequireComponent(typeof(Renderer))]
    public class MaterialInstanceProvider : MonoBehaviour
    {
        struct SharedMaterialData
        {
            public Material Instance;
            public int Used;
        }
        static readonly Dictionary<Material, SharedMaterialData> k_staticMap = new Dictionary<Material, SharedMaterialData>();

#pragma warning disable 0649 // wrong warnings for SerializeField 
        [SerializeField] bool m_shareMaterialStatic;
#pragma warning restore 0649 // wrong warnings for SerializeField

        Renderer m_renderer;

        Material[] m_originalMaterials;
        Material[] m_instancedMaterials;

        public void GetInstances(out Material[] origMaterials, out Material[] instanceMaterials)
        {
            origMaterials = m_originalMaterials;
            instanceMaterials = m_instancedMaterials;
            if (instanceMaterials != null && origMaterials!= null)
                return;

            var sharedMaterials = m_renderer.sharedMaterials;
            m_originalMaterials = new Material[sharedMaterials.Length];
            m_instancedMaterials = new Material[sharedMaterials.Length];

            if (m_shareMaterialStatic)
            {
                GetInstancesStatic(out origMaterials, out instanceMaterials);
                return;
            }

            for (var i = 0; i < sharedMaterials.Length; i++)
            {
                m_originalMaterials[i] = sharedMaterials[i];
                m_instancedMaterials[i] = Instantiate(sharedMaterials[i]);
            }
            origMaterials = m_originalMaterials;
            instanceMaterials = m_instancedMaterials;
        }

        void GetInstancesStatic(out Material[] origMaterials, out Material[] instanceMaterials)
        {
            var sharedMaterials = m_renderer.sharedMaterials;

            for (var i = 0; i < m_renderer.sharedMaterials.Length; i++)
            {
                m_originalMaterials[i] = sharedMaterials[i];

                if (k_staticMap.TryGetValue(sharedMaterials[i], out var data))
                {
                    m_instancedMaterials[i] = data.Instance;
                    var newUsed = data.Used + 1;
                    k_staticMap[sharedMaterials[i]] 
                        = new SharedMaterialData() { Instance = m_instancedMaterials[i], Used = newUsed };
                    continue;
                }
                var newMatInstance = Instantiate(sharedMaterials[i]);
                k_staticMap.Add(sharedMaterials[i], new SharedMaterialData(){ Instance = newMatInstance, Used = 1 });
                m_instancedMaterials[i] = newMatInstance;
            }
            origMaterials = m_originalMaterials;
            instanceMaterials = m_instancedMaterials;
        }

        void OnEnable()
        {
            if (!TryGetComponent(out Renderer r))
                return;

            m_renderer = r;
        }

        void OnDisable()
        {
            if (m_instancedMaterials == null)
                return;

            if (m_renderer != null)
                m_renderer.sharedMaterials = m_originalMaterials;

            if (m_shareMaterialStatic)
            {
                ReleaseMaterialsShared();
                return;
            }

            DestroyMaterials();
        }

        void ReleaseMaterialsShared()
        {
            if (m_originalMaterials.IsNullOrEmpty())
                return;

            foreach (var mat in m_originalMaterials)
            {
                if (!k_staticMap.TryGetValue(mat, out var data)) 
                    continue;
                var inst = data.Instance;
                var newUsed = data.Used - 1;
                if (newUsed == 0)
                {
                    inst.DestroyEx();
                    k_staticMap.Remove(mat);
                    continue;
                }
                k_staticMap[mat] 
                    = new SharedMaterialData() { Instance = inst, Used = newUsed };
            }
            m_instancedMaterials = null;
        }

        void DestroyMaterials()
        {
            foreach (var m in m_instancedMaterials)
                m.DestroyEx();

            m_instancedMaterials = null;
        }
    }
}
