﻿using System;

using GameUtility.Data.TransformAttachment;
using GameUtility.Data.Visual;
using ScriptableUtility;
using UnityEngine;
using UnityEngine.Serialization;

using ObsoleteDrawer = Core.Unity.Types.Fsm.Obsolete.FSMDrawerAttribute;
using ObsoleteFSMGameObjectRef = Core.Unity.Types.Fsm.Obsolete.FSMGameObjectRef;

namespace GameUtility.Data.FX
{
    [Serializable]
    public struct FXLink
    {
        public FX_ID ID;
        public AttachmentOrTransformData TransformData;

        [FormerlySerializedAs("Ref")] 
        [ObsoleteDrawer(editVariable: false)]
        public ObsoleteFSMGameObjectRef RefObsolete;

        public VarReference<GameObject> VarRef;

        public bool DontParent;

        public bool InitialActive;
        [HideInInspector] public GameObject Instance;
        [HideInInspector] public GameObject Prefab;
    }
}