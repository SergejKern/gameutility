﻿using GameUtility.Data.Visual;
using UnityEngine;

namespace GameUtility.Data.FX
{
    public interface IFXComponent
    {
        GameObject GetInstance(FX_ID id);
    }
}
