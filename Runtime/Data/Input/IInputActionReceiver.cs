using System.Collections.Generic;

namespace GameUtility.Data.Input
{
    public interface IInputActionReceiver
    {
        IReadOnlyList<ActionMapping> Mapping { get; }
    }
}