using System;
using Core.Unity.Types;

namespace GameUtility.Data.Input
{
    // ReSharper disable once UnusedMemberInSuper.Global
    public interface IInputTrigger
    {
        void Trigger();
    }

    [Serializable]
    public class RefIInputTrigger: InterfaceContainer<IInputTrigger> { }
}