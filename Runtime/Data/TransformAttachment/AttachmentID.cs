﻿using Core.Unity.Attributes;
using Core.Unity.Types.ID;

namespace GameUtility.Data.TransformAttachment
{
    [EditorIcon("icon-id")]
    public class AttachmentID : IDAsset { }
}
