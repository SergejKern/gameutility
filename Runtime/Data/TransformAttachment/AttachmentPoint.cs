﻿using System;

namespace GameUtility.Data.TransformAttachment
{
    [Serializable]
    public struct AttachmentPoint
    {
        public AttachmentID ID;
        public TransformData TransformData;
    }
}
