﻿using System;
using UnityEngine;

namespace GameUtility.Data.TransformAttachment
{
    [Serializable]
    public struct TransformData : ITransformData
    {
        public Transform Parent;
        public Vector3 LocalPosition;
        public Vector3 LocalRotation;

        public Vector3 Position => Parent == null ? LocalPosition : Parent.TransformPoint(LocalPosition);
        public Quaternion Rotation => Parent == null ? Quaternion.Euler(LocalRotation) : Parent.rotation * Quaternion.Euler(LocalRotation);
    }
}
