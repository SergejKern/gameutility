using Core.Unity.Attributes;
using Core.Unity.Types.ID;

namespace GameUtility.Data.Debugging
{
    [EditorIcon("icon-id")]
    public class DebugDrawID : IDAsset { }
}