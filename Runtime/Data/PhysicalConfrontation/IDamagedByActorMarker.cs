﻿using UnityEngine;

namespace GameUtility.Data.PhysicalConfrontation
{
    public interface IDamagedByActorMarker
    {
        // ReSharper disable once UnusedMemberInSuper.Global
        GameObject LastAggressor { get; set; }
    }
}