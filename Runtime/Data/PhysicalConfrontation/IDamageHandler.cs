namespace GameUtility.Data.PhysicalConfrontation
{
    public interface IDamageHandler : ICollisionMaterialComponent
    {
        void HandleDamage(float damage, ref HandleDamageResult result);
    }
}
