using Core.Unity.Attributes;
using Core.Unity.Types.ID;

namespace GameUtility.Data.PhysicalConfrontation
{
    [EditorIcon("icon-id")]
    public class TeamFlagID : IDAsset { }
}