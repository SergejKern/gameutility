using System;
using Core.Unity.Types;

namespace GameUtility.Data.EntityAnima
{
    public interface IReactToModelInitialized
    {
        void OnModelInitialized();
    }

    [Serializable]
    public class RefIReactToModelInitialized : InterfaceContainer<IReactToModelInitialized> { }
}