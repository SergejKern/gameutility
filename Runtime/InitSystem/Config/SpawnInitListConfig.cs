﻿using System.Collections.Generic;
using Core.Events;
using GameUtility.PooledEventSpawning;
using UnityEngine;

namespace GameUtility.InitSystem.Config
{
    // todo 1: add sliders, tooltips and infos for Game Settings & Feature-Toggles
    [CreateAssetMenu(menuName = "Game/SpawnInitListConfig")]
    public class SpawnInitListConfig : ScriptableObject, IEventListener<PrefabSpawnedEvent>
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField]
        internal List<SpawnInitConfig> m_initConfigs = new List<SpawnInitConfig>();
#pragma warning restore 0649 // wrong warnings for SerializeField

        public List<SpawnInitConfig> InitConfigs => m_initConfigs;
        readonly Dictionary<GameObject, List<RefIInitializationConfig>> m_spawnInitMap = new Dictionary<GameObject, List<RefIInitializationConfig>>();

        void OnEnable()
        {
            m_spawnInitMap.Clear();
            foreach (var ic in m_initConfigs)
                m_spawnInitMap.Add(ic.Prefab, ic.Configs);
        }

        public void StartListen() => this.EventStartListening();
        public void StopListen() => this.EventStopListening();

        public void OnEvent(PrefabSpawnedEvent spawnEvent)
        {
            if (!m_spawnInitMap.TryGetValue(spawnEvent.FromPrefab, out var initConfigList))
                return;

            foreach (var initConfig in initConfigList)
            {
                var component = spawnEvent.SpawnedObject.GetComponentInChildren(initConfig.Result.ComponentType, true);
                initConfig.Result.InitializeComponent(component);
            }
        }

        //public static string InitConfigsProperty => nameof(m_initConfigs);
    }

}
