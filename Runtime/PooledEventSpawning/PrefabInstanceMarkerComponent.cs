﻿using Core.Unity.Interface;
using UnityEngine;

namespace GameUtility.PooledEventSpawning
{
    public class PrefabInstanceMarkerComponent : MonoBehaviour, IPrefabInstanceMarker
    {
        public GameObject Prefab { get; set; }
    }
}
