﻿using System.Collections;
using UnityEngine;

namespace GameUtility.Components.Transform
{
    /// <summary>
    /// Component with routine to start vibrate like a gong or bell
    /// [RC] = Routine Component
    /// </summary>
    public class VibrateRC : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] Vector3 m_speed = Vector3.forward;
        [SerializeField] Vector3 m_amount = Vector3.forward;

        [SerializeField] Vector3 m_offset = Vector3.forward;
        [SerializeField] float m_time;
#pragma warning restore 0649 // wrong warnings for SerializeField

        Quaternion m_restRotation;

        Vector3 m_vibrationUpdate;

        public void Vibrate() => StartCoroutine(DoVibrate());

        IEnumerator DoVibrate()
        {
            m_vibrationUpdate = m_offset;
            m_restRotation = transform.rotation;
            var amount = m_amount;
            var timer = 0f;
            while (timer < m_time)
            {
                UpdateVibration(amount);

                var progress = timer / m_time;
                amount = Vector3.Slerp(m_amount, Vector3.zero, progress * progress);
                timer += Time.deltaTime;
                yield return null;
            }
        }

        void UpdateVibration(Vector3 amount)
        {
            var sinX= Mathf.Sin(m_vibrationUpdate.x);
            var sinY= Mathf.Sin(m_vibrationUpdate.y);
            var sinZ= Mathf.Sin(m_vibrationUpdate.z);

            m_vibrationUpdate += Time.deltaTime * m_speed;
            transform.rotation = m_restRotation * Quaternion.Euler(sinX * amount.x, sinY * amount.y, sinZ * amount.z);
        }
    }
}
