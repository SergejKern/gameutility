using UnityEngine;
using UnityEngine.Serialization;

namespace GameUtility.Components.Transform
{
    public class BillboardBehaviour : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] bool m_lookAt = true;
        [SerializeField] bool m_copyRotation;
        [SerializeField] Vector3 m_eulerOffset;
#pragma warning restore 0649 // wrong warnings for SerializeField

        // Update is called once per frame
        void Update()
        {
            if (Camera.main == null)
                return;
            if (m_lookAt)
                transform.LookAt(Camera.main.transform);
            CopyRotation();
        }

        void CopyRotation()
        {
            if (!m_copyRotation)
                return;
            if (Camera.main == null)
                return;

            var euler = Camera.main.transform.rotation.eulerAngles;
            transform.rotation = Quaternion.Euler(euler.x + m_eulerOffset.x, euler.y + m_eulerOffset.y, euler.z + m_eulerOffset.z);
        }
    }
}
