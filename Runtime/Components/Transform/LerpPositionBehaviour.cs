﻿using GameUtility.Data.TransformAttachment;
using UnityEngine;

namespace GameUtility.Components.Transform
{
    public class LerpPositionBehaviour : MonoBehaviour
    {
#pragma warning disable 0649
        [SerializeField] float m_speed;
#pragma warning restore 0649

        public TransformData Follow;
        
        void Update()
        {
            if (Follow.Parent == null)
                return;

            var tr = transform;
            tr.position = Vector3.Lerp(tr.position, Follow.Position, m_speed * Time.deltaTime);
        }
    }
}
