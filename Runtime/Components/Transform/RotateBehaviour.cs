using UnityEngine;
using UnityEngine.Serialization;

namespace GameUtility.Components.Transform
{
    public class RotateBehaviour : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] float m_degreesPerSecond = 360;
        [SerializeField] Vector3 m_axis = Vector3.forward;
#pragma warning restore 0649 // wrong warnings for SerializeField

        UnityEngine.Transform m_transform;

        Quaternion m_lastRotation;

        void Awake()
        {
            m_transform = transform;
            m_lastRotation = m_transform.localRotation;

            m_axis = Vector3.Normalize(m_axis);
        }

        void Update()
        {
            var localRotation = m_lastRotation;
            localRotation *= Quaternion.AngleAxis(m_degreesPerSecond * Time.deltaTime, m_axis);
            m_transform.localRotation = localRotation;
            m_lastRotation = localRotation;
        }
    }
}
