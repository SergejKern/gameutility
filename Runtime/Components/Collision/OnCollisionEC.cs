using Core.Unity.Types;
using UnityEngine;
using UnityEngine.Events;

namespace GameUtility.Components.Collision
{
    /// <summary>
    /// UnityEvents (GameObjectEvent) for OnCollisionEnter, OnCollisionStay, OnCollisionExit
    /// [EC] = Event Component
    /// </summary>
    public class OnCollisionEC : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField]
        UnityEvent<GameObject> m_onCollisionEnterEvent;
        [SerializeField]
        UnityEvent<GameObject> m_onCollisionStayEvent;
        [SerializeField]
        UnityEvent<GameObject> m_onCollisionExitEvent;
#pragma warning restore 0649 // wrong warnings for SerializeField

        void OnCollisionEnter(UnityEngine.Collision other)
        {
            if (other != null)
                m_onCollisionEnterEvent?.Invoke(RelativeCollider.GetRoot(other));
        }
        void OnCollisionStay(UnityEngine.Collision other)
        {
            if (other != null)
                m_onCollisionStayEvent?.Invoke(RelativeCollider.GetRoot(other));
        }

        void OnCollisionExit(UnityEngine.Collision other)
        {
            if (other != null)
                m_onCollisionExitEvent?.Invoke(RelativeCollider.GetRoot(other));
        }
    }
}
