﻿using UnityEngine;
using UnityEngine.Serialization;

namespace GameUtility.Components.Collision
{
    /// <inheritdoc />
    /// <summary>
    /// A collider that can be arranged as a child several layers deep.
    /// DO use if other objects need to know of the associatedObject instead of the collider object.
    /// DON'T use in conjunction with RelativeTriggerForwarding or RelativeCollisionForwarding,
    /// just because associatedObject needs to know of collision or trigger-event
    /// </summary>
    [RequireComponent(typeof(Collider))]
    public class RelativeCollider : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] GameObject m_associatedObject;
#pragma warning restore 0649 // wrong warnings for SerializeField

        // ReSharper disable once ConvertToAutoProperty, ConvertToAutoPropertyWhenPossible, ConvertToAutoPropertyWithPrivateSetter
        public GameObject AssociatedObject => m_associatedObject;
        public void SetAssociatedObject(GameObject obj) => m_associatedObject = obj;
        public static GameObject GetRoot(Collider col) => GetRoot(col.gameObject);
        public static GameObject GetRoot(UnityEngine.Collision col) => GetRoot(col.gameObject);
        public static GameObject GetRoot(GameObject go)
        {
            if (go == null)
                return null;
            var collisionRoot = go;
            if (collisionRoot.TryGetComponent<RelativeCollider>(out var relative))
                collisionRoot = relative.AssociatedObject;
            return collisionRoot;
        }
    }


}
