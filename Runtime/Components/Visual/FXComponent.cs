﻿using System;
using Core.Unity.Extensions;
using Core.Unity.Utility.PoolAttendant;
using GameUtility.Data.FX;
using GameUtility.Data.TransformAttachment;
using GameUtility.Data.Visual;
using UnityEngine;

namespace GameUtility.Components.Visual
{
    public class FXComponent : MonoBehaviour, IUpdateAttachmentPoints, IFXComponent
    {
#pragma warning disable 0649
        [SerializeField]
        protected FXLink[] m_links = new FXLink[0];
#pragma warning restore 0649 // wrong warnings for SerializeField

        ref FXLink GetLink(int i) => ref m_links[i];

        public void UpdateAttachmentPoints(AttachmentPointsProvider provider)
        {
            for (var i = 0; i < m_links.Length; i++)
            {
                ref var fxLink = ref GetLink(i);
                if (fxLink.TransformData.UseID)
                    fxLink.TransformData.TransformData = provider.GetAttachment(fxLink.TransformData.ID);
            }
        }

        public GameObject GetInstance(FX_ID id)
        {
            var link = Array.Find(m_links, p => p.ID == id);

            if (link.Instance != null)
            {
                if (link.Instance.activeInHierarchy) 
                    return link.Instance;
            
                link.Instance.TryDespawnOrDestroy();
            }

            var td = link.TransformData;
            link.Instance = link.Prefab.GetPooledInstance(td.Position, td.Rotation);

            if (!link.DontParent)
                link.Instance.transform.SetParent(td.Parent, false);

            link.Instance.transform.position = td.Position;
            link.RefObsolete.Value = link.Instance;
            link.VarRef.Value = link.Instance;

            return link.Instance;
        }
    }
}