﻿using System.Collections.Generic;
using Core.Unity.Extensions;
using Core.Unity.Types;
using Core.Unity.Utility.PoolAttendant;
using UnityEngine;
using UnityEngine.Serialization;

namespace GameUtility.Components
{
    // ReSharper disable once IdentifierTypo
    public class Despawner : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [Range(0.05f, 30)]
        [SerializeField] float m_seconds;

        // todo 3 extract to another component
        [SerializeField] List<Renderer> m_blinkingRenderer;
        [SerializeField] float m_blinkRateA = 5f;
        [SerializeField] float m_blinkRateB = 20f;

        [SerializeField] PrefabData m_despawnEffect;
#pragma warning restore 0649 // wrong warnings for SerializeField

        float m_time;

        public void SetSeconds(float s)
        {
            m_time = 0;
            m_seconds = s;
        }
        void OnEnable() => m_time = 0;

        void OnDisable()
        {
            m_time = 0;
            SetRendererActive(true);
        }

        void Update()
        {
            m_time += Time.deltaTime;

            var percent = m_time / m_seconds;

            var blinkRate = 0f;
            if (percent > 0.8f)
                blinkRate = m_blinkRateB;
            else if (percent > 0.33f)
                blinkRate = m_blinkRateA;

            var active = (int)(m_time * blinkRate) % 2 == 0;
            SetRendererActive(active);

            if (m_time > m_seconds)
                Despawn();
        }

        void SetRendererActive(bool active)
        {
            if (m_blinkingRenderer == null)
                return;
            foreach (var r in m_blinkingRenderer)
            {
                if (r == null)
                    continue;
                r.enabled = active;
            }

        }

        void Despawn()
        {
            if (!enabled)
                return;

            var pos = transform.position;
            gameObject.TryDespawnOrDestroy();

            var despawnPrefab = m_despawnEffect.Prefab;
            if (despawnPrefab != null)
                despawnPrefab.GetPooledInstance(pos);
        }
    }
}