﻿using Core.Unity.Interface;
using UnityEngine;

namespace GameUtility.Components.Actions
{
    /// <summary>
    /// Action for playing sounds
    /// [AC] = Action Component
    /// </summary>
    public class PlaySoundAC : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] RefIAudioAsset m_audioAsset;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public void PlaySound() => m_audioAsset?.Result?.Play(gameObject);
    }
}
