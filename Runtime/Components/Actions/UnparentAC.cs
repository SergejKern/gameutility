﻿using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace GameUtility.Components.Actions
{
    /// <summary>
    /// Action for unparenting transforms
    /// [AC] = Action Component
    /// </summary>
    public class UnparentAC : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] List<UnityEngine.Transform> m_unparentTransforms;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public void AddTransform(UnityEngine.Transform tr)
        {
            if (m_unparentTransforms == null)
                m_unparentTransforms = new List<UnityEngine.Transform>();
            m_unparentTransforms.Add(tr);
        }

        // Update is called once per frame
        public void DoUnparent()
        {
            foreach (var tr in m_unparentTransforms)
                tr.SetParent(null, true);
        }

        [UsedImplicitly]
        public void Clear() => m_unparentTransforms.Clear();
    }
}
