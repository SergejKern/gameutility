﻿using Core.Unity.Interface;
using Core.Unity.Types;
using Core.Unity.Utility.PoolAttendant;
using GameUtility.Data.TransformAttachment;
using UnityEngine;

namespace GameUtility.Components.Actions
{
    /// <summary>
    /// Action for playing effects
    /// [AC] = Action Component
    /// </summary>
    public class PlayEffectAC : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] RefIAudioAsset m_audioAsset;
        [SerializeField] PrefabData m_fX;
        [SerializeField] TransformData m_transformData; 
#pragma warning restore 0649 // wrong warnings for SerializeField

        public void PlayEffect()
        {
            m_audioAsset?.Result?.Play(gameObject);
            var prefab = m_fX.Prefab;
            if (prefab != null)
                prefab.GetPooledInstance(m_transformData.Position);
        }
    }
}
