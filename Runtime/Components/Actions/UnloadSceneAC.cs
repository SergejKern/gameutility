﻿using System.Collections;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace GameUtility.Components.Actions
{
    /// <summary>
    /// Action for unloading scene
    /// [AC] = Action Component
    /// </summary>
    public class UnloadSceneAC : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] string m_unloadScene;
        [SerializeField] UnityEvent m_onSceneUnloaded;
#pragma warning restore 0649 // wrong warnings for SerializeField

        [UsedImplicitly]
        public void Trigger() => StartCoroutine(UnloadAsync());
        IEnumerator UnloadAsync()
        {
            var asyncOp = SceneManager.UnloadSceneAsync(m_unloadScene);
            while (!asyncOp.isDone)
                yield return null;
            
            m_onSceneUnloaded.Invoke();
        }
    }
}
