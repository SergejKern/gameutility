﻿using System.Collections.Generic;
using GameUtility.Data.Visual;
using UnityEngine;

namespace GameUtility.Components.Actions
{
    /// <summary>
    /// Action for changing materials
    /// [AC] = Action Component
    /// </summary>
    public class MaterialExchangeAC : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] MaterialOverrideMap[] m_overrideMap;
        [SerializeField] bool m_revertOnDisable;
        [SerializeField] GameObject m_applyOnGameObject;
#pragma warning restore 0649 // wrong warnings for SerializeField

        bool m_exchanged;

        IMemorizeMaterials m_memoryComp;
        readonly List<MaterialMemory> m_memory = new List<MaterialMemory>();
        List<MaterialMemory> Memory => m_memoryComp != null ? m_memoryComp.Memory : m_memory;

        void Awake()
        {
            m_memoryComp = GetComponent<IMemorizeMaterials>();
            if (m_applyOnGameObject == null)
                m_applyOnGameObject = gameObject;
        }

        public void Exchange()
        {
            m_exchanged = true;
            MaterialExchange.Override(m_applyOnGameObject, m_overrideMap, Memory);
        }

        public void Revert()
        {
            m_exchanged = false;
            MaterialExchange.Revert(Memory);
            Memory.Clear();
        }

        void OnDisable()
        {
            if (m_exchanged && m_revertOnDisable)
                Revert();
        }

        //public void Clear() => m_destroyComponents.Clear();
    }
}
