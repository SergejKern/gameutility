﻿using UnityEngine;
using UnityEngine.Events;

namespace GameUtility.Components.Events
{
    /// <summary>
    /// Unity Event on Start()
    /// [EC] = Event Component
    /// </summary>
    public class StartEC : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] UnityEvent m_startEvent;
#pragma warning restore 0649 // wrong warnings for SerializeField

        void Start() => m_startEvent.Invoke();
    }
}
