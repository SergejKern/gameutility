﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace GameUtility.Components.Events
{
    // todo 2: Review this, make sure it is easy to use and understand and not too specific
    /// <summary>
    /// Unity Event once rigidbody is resting (velocity)
    /// will disable itself once fired
    /// [EC] = Event Component
    /// </summary>
    [RequireComponent(typeof(Rigidbody))]
    public class RigidbodyOnRestEC : MonoBehaviour
    {
        Rigidbody m_rigidBodyCached;

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] float m_actionWhenVelocityMagnitudeLower = 0.25f;
        [SerializeField] float m_rayCastGroundCheckDistance = 0.25f;

        [SerializeField] UnityEvent m_onRest;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public void AddOnRestListener(UnityAction call) => m_onRest.AddListener(call);

        void OnEnable()
        {
            m_rigidBodyCached = GetComponent<Rigidbody>();
            StartCoroutine(CheckResting());
        }

        IEnumerator CheckResting()
        {
            yield return new WaitForEndOfFrame();

            while (enabled)
            {
                yield return new WaitForEndOfFrame();

                if (m_rigidBodyCached.velocity.magnitude > m_actionWhenVelocityMagnitudeLower)
                    continue;

                if (!Physics.Raycast(transform.position, Vector3.down, m_rayCastGroundCheckDistance))
                    continue;

                break;
            }

            if (!enabled)
                yield break;

            enabled = false;
            m_onRest.Invoke();
        }
    }
}
