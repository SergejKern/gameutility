﻿using UnityEngine;
using UnityEngine.Events;

namespace GameUtility.Components.Events
{
    /// <summary>
    /// Unity Event on awake
    /// [EC] = Event Component
    /// </summary>
    public class AwakeEC : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] UnityEvent m_awakeEvent;
#pragma warning restore 0649 // wrong warnings for SerializeField

        void Awake()
        {
            if (enabled)
                m_awakeEvent.Invoke();
        }
    }
}
