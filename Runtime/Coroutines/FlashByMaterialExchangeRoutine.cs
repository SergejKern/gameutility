﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Types;
using GameUtility.Data.Visual;
using UnityEngine;

namespace GameUtility.Coroutines
{
    [Serializable]
    public struct FlashByMaterialExchangeData
    {
        public MaterialExchangeData MapData;
        public float FlashDuration;
        public List<MaterialMemory> Memory;
    }

    [Serializable]
    public struct FlashByMaterialExchangeData2
    {
        public MaterialExchangeData2 MapData;
        public float FlashDuration;
        public List<MaterialMemory> Memory;
    }

    [Serializable]
    public struct FlashDataNoTarget
    {
        public MaterialOverrideMap[] MaterialOverrideMap;
        public float FlashDuration;
        public List<MaterialMemory> Memory;
    }

    public static class FlashByMaterialExchangeRoutine
    {
        public static IEnumerator Flash(FlashDataNoTarget data, GameObject model)
        {
            if (model == null)
                yield break;
            var r = model.GetComponentsInChildren<Renderer>();

            yield return Flash(
            new FlashByMaterialExchangeData()
            {
                FlashDuration = data.FlashDuration,
                Memory = data.Memory,
                MapData = new MaterialExchangeData()
                {
                    Renderer = r,
                    MaterialMap = data.MaterialOverrideMap
                }
            });
        }

        /// <summary>
        /// Coroutine used to make renderer flicker (when actor is hurt for example).
        /// </summary>
        public static IEnumerator Flash(FlashByMaterialExchangeData2 data)
        {
            if (data.MapData.ForObject == null)
                yield break;
            var r = data.MapData.ForObject.GetComponentsInChildren<Renderer>();
            yield return Flash(new FlashByMaterialExchangeData()
            {
                FlashDuration = data.FlashDuration,
                Memory = data.Memory,
                MapData = new MaterialExchangeData()
                {
                    Renderer = r,
                    MaterialMap = data.MapData.MaterialMap
                }
            });
        }

        /// <summary>
        /// Coroutine used to make renderer flicker (when actor is hurt for example).
        /// </summary>
        static IEnumerator Flash(FlashByMaterialExchangeData data)
        {
            if (data.MapData.Renderer == null)
                yield break;
            if (data.MapData.MaterialMap == null)
                yield break;
            using (var mem = SimplePool<List<MaterialMemory>>.I.GetScoped())
            {
                var memory = data.Memory ?? mem.Obj;
                MaterialExchange.Override(data.MapData, memory);
                yield return new WaitForSeconds(data.FlashDuration);
                MaterialExchange.Revert(memory);
                memory.Clear();
            }
        }

    }
}