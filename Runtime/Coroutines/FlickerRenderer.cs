﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameUtility.Data.Visual;
using UnityEngine;

namespace GameUtility.Coroutines
{
    [Serializable]
    public struct FlickerData
    {
        public Renderer[] Renderer;
        public float Frequency;
        public float FlickerDuration;
    }

    [Serializable]
    public struct FlickerMaterialData
    {
        public Renderer[] Renderer;
        public float Frequency;
        public float FlickerDuration;
        public MaterialOverrideMap[] Override;
    }

    public static class FlickerRoutine
    {
        /// <summary>
        /// Coroutine used to make renderer flicker (when actor is hurt for example).
        /// </summary>
        public static IEnumerator Flicker(FlickerData data)
        {
            if (data.Renderer == null)
                yield break;

            var time = data.FlickerDuration;
            var on = false;
            while (time > 0)
            {
                foreach (var r in data.Renderer)
                    r.enabled = on;
                
                yield return new WaitForSeconds(Mathf.Min(data.Frequency, time));
                time -= data.Frequency;
                on=!on;
            }
            foreach (var r in data.Renderer)
                r.enabled = true;
        }

        /// <summary>
        /// Coroutine used to flicker by changing material.
        /// </summary>
        public static IEnumerator Flicker(FlickerMaterialData data,  List<MaterialMemory> memory)
        {
            if (data.Renderer == null)
                yield break;

            var time = data.FlickerDuration;
            var on = true;
            while (time > 0)
            {
                if (on)
                    MaterialExchange.Override(data.Renderer, data.Override, memory);
                else 
                    MaterialExchange.Revert(memory);

                yield return new WaitForSeconds(Mathf.Min(data.Frequency, time));
                time -= data.Frequency;
                on=!on;
            }
            MaterialExchange.Revert(memory);
        }
    }
}