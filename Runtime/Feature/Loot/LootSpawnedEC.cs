﻿using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;

namespace GameUtility.Feature.Loot
{
    /// <summary>
    /// Event Component for when loot has been spawned
    /// EC = Event Component
    /// </summary>
    public class LootSpawnedEC : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] UnityEvent m_onLootSpawned;
#pragma warning restore 0649 // wrong warnings for SerializeField

        [UsedImplicitly]
        public void OnLootSpawned()
        {
            m_onLootSpawned.Invoke();
        }
    }
}
