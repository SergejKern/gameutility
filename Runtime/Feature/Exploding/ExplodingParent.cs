﻿using System.Collections;
using Core.Interface;
using UnityEngine;

namespace GameUtility.Feature.Exploding
{
    public class ExplodingParent : MonoBehaviour, IPoolable
    {
        public bool DestroyThisOnRestore = true;
        public ExplodingPartMemory[] Parts;

        Coroutine m_restoringRoutine;

        IEnumerator Restore()
        {
            if (Parts == null || this == null)
                yield break;

            foreach (var p in Parts)
            {
                if (p == null || p.HadRigidbody)
                    continue;
                p.DestroyRigidbody();
            }
            yield return null;
            foreach (var p in Parts) 
                p.Restore();
            // todo: this Animator-stuff seems wrong here: (probably only for enemies)
            //if (gameObject.TryGetComponent<Animator>(out var animator))
            //    animator.enabled = true;
            if (DestroyThisOnRestore)
                Destroy(this);
            else 
                Parts = null;

            m_restoringRoutine = null;
        }

        public void DoRestore()
        {
            if (m_restoringRoutine != null)
            {
                Debug.LogWarning($"Already restoring! {gameObject.name}");
                return;
            }

            m_restoringRoutine = StartCoroutine(Restore());
        }

        public void OnSpawn() {}
        public void OnDespawn() => DoRestore();
    }
}
