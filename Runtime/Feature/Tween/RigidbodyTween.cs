﻿using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

namespace GameUtility.Feature.Tween
{
    public static class RigidbodyTween
    {
        // ReSharper disable once InconsistentNaming
        public static TweenerCore<Vector3, Vector3, VectorOptions> DOMove(
            this Rigidbody target,
            Vector3 endValue,
            float duration,
            bool snapping = false)
        {
            var t = DOTween.To(() => target.position, target.MovePosition, endValue, duration);
            t.SetOptions(snapping).SetTarget(target);
            t.SetUpdate(UpdateType.Fixed);

            return t;
        }

        // ReSharper disable once InconsistentNaming
        public static TweenerCore<Quaternion, Vector3, QuaternionOptions> DORotate(
            this Rigidbody target,
            Vector3 endValue,
            float duration,
            RotateMode mode = RotateMode.Fast)
        {
            var t = DOTween.To(() => target.rotation, target.MoveRotation, endValue, duration);
            t.SetTarget(target);
            t.plugOptions.rotateMode = mode;
            t.SetUpdate(UpdateType.Fixed);

            return t;
        }
    }
}