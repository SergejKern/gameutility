﻿using Core.Unity.Attributes;
using Core.Unity.Types.ID;

namespace GameUtility.Feature.Loading
{
    [EditorIcon("icon-id")]
    public class LoadOperationID : IDAsset { }
}
