﻿using Core.Interface;
using GameUtility.Energy;

namespace GameUtility.Interface
{
    public interface IRevivable : IProvider<IHealthComponent>
    {
        float TimeToRevive { get; }
        void OnRevive();
    }
}
