using UnityEngine;

namespace GameUtility.Interface
{
    /// <summary>
    /// interactive objects should also have a IHighlightable component and will be highlighted when in range
    /// </summary>
    public interface IInteractive
    {
        bool InteractEnabled { get; }
    }

    /// <inheritdoc />
    /// <summary>
    /// IInteractive objects that can be triggered by pressing the interaction button
    /// </summary>
    public interface IInteractivePress : IInteractive
    {
        void Interact(GameObject actor);
    }

    public interface IInteractiveHolding : IInteractivePress
    {
        void OnRelease(GameObject actor);
    }
}
