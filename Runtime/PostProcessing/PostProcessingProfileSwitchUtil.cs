﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace GameUtility.PostProcessing
{
    public static class PostProcessingProfileSwitchUtil
    {
        public static PostProcessVolume Switch(PostProcessProfile profile)
        {
            if (Camera.main == null)
                return null;

            var camGo = Camera.main.gameObject;

            var volume = camGo.AddComponent<PostProcessVolume>();
            volume.isGlobal = true;
            volume.weight = 1.0f;
            volume.profile = profile;
            return volume;
        }
    }
}
