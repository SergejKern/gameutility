
using UnityEngine;

namespace GameUtility.Validation
{
    public interface IAnimatorStateValidation
    {
        RuntimeAnimatorController AnimatorController { get; }
        string[] States { get; }
    }
    public struct AnimatorStateValidationData : IAnimatorStateValidation
    {
        public RuntimeAnimatorController AnimatorController { get; set; }
        public string[] States { get; set; }
    }

    public interface IAnimatorCollectionStateValidation
    {
        IAnimatorStateValidation[] Data { get; }
    }
}