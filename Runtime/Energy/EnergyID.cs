﻿using Core.Unity.Attributes;
using Core.Unity.Types.ID;

namespace GameUtility.Energy
{
    [EditorIcon("icon-id")]
    public class EnergyID : IDAsset { }
}
