using Core.Events;
using UnityEngine;

namespace GameUtility.Events
{
    public struct KillEvent
    {
        public readonly GameObject KilledActor;
        public readonly GameObject Killer;

        KillEvent(GameObject killedActor, GameObject killer)
        {
            KilledActor = killedActor;
            Killer = killer;
        }

        public static void Trigger(GameObject killedActor, GameObject killer) => Trigger(new KillEvent(killedActor, killer));
        static void Trigger(KillEvent e) => EventMessenger.TriggerEvent(e);
    }
}
