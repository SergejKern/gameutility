using Core.Events;
using GameUtility.Interface;
using UnityEngine;

namespace GameUtility.Events
{
    public struct AttackDataChangedEvent
    {
        public readonly GameObject Originator;
        public readonly IAttackDataProvider AttackDataProvider;

        AttackDataChangedEvent(GameObject originator, IAttackDataProvider attackDataProvider)
        {
            Originator = originator;
            AttackDataProvider = attackDataProvider;
        }
        public static void Trigger(GameObject originator, IAttackDataProvider provider) => Trigger(new AttackDataChangedEvent(originator, provider));
        static void Trigger(AttackDataChangedEvent e) => EventMessenger.TriggerEvent(e);
    }
}
