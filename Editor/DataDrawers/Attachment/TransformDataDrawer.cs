﻿using System;
using System.Collections.Generic;
using Core.Editor.Utility.GUITools;
using GameUtility.Data.TransformAttachment;
using UnityEditor;
using UnityEngine;

using static Core.Editor.Extensions.SerializedPropertyExtensions;
using Object = UnityEngine.Object;

namespace _Editor.GameUtility.Data.Attachment
{
#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(TransformData))]
    public class TransformDataDrawer : PropertyDrawer
    {
        struct TransformDataProp
        {
            public string LastSelectedPropertyPath;
            public SerializedObject LastSerializedObject;
            public Object LastSelected;
        }

        static bool m_onSceneGUIRegistered;
        static TransformDataProp m_selection;
        static readonly List<string> k_passivePaths = new List<string>();

        void Select(SerializedProperty property)
        {
            if (m_selection.LastSerializedObject != property.serializedObject)
                k_passivePaths.Clear();

            m_selection.LastSelectedPropertyPath = property.propertyPath;
            m_selection.LastSerializedObject = property.serializedObject;
            m_selection.LastSelected = Selection.activeObject;

            AddSceneGUI();
        }

        void Deselect()
        {
            m_selection = default;
            k_passivePaths.Clear();

            RemoveSceneGUI();
        }

        void AddSceneGUI()
        {
            if (m_onSceneGUIRegistered)
                return;

            m_onSceneGUIRegistered = true;
            SceneView.duringSceneGui += DuringSceneGUI;
        }

        void RemoveSceneGUI()
        {
            Debug.Log("RemoveSceneGUI");
            if (!m_onSceneGUIRegistered)
                return;

            m_onSceneGUIRegistered = false;
            SceneView.duringSceneGui -= DuringSceneGUI;
        }

        void DuringSceneGUI(SceneView obj)
        {
            if (Selection.activeObject == null 
                || Selection.activeObject != m_selection.LastSelected)
            {
                Deselect();
                return;
            }

            var sObj = m_selection.LastSerializedObject;
            var propPath = m_selection.LastSelectedPropertyPath;
            if (sObj == null || propPath == null)
                return;

            var prop = sObj.FindProperty(propPath);
            var td = prop.GetValue<TransformData>();

            DrawSelectedHandles(td, prop, sObj);
            
            DrawPassives(propPath, sObj);
        }

        static void DrawPassives(string propPath, SerializedObject sObj)
        {
            foreach (var passivePath in k_passivePaths)
            {
                if (string.Equals(passivePath, propPath))
                    continue;

                var passiveProp = sObj.FindProperty(passivePath);
                var td = passiveProp.GetValue<TransformData>();
                var pos = td.Position;
                var rot = td.Rotation;
                CustomHandles.PositionHandle(pos, rot,
                    Color.gray, Color.gray, Color.gray, 0.5f);
            }
        }

        static void DrawSelectedHandles(TransformData td, SerializedProperty prop, SerializedObject sObj)
        {
            var pos = td.Position;
            var rot = td.Rotation;

            switch (Tools.current)
            {
                case Tool.Move:
                {
                    var newPos = CustomHandles.PositionHandle(pos, rot,
                        Color.magenta, Color.yellow, Color.cyan, 0.5f);
                    if (pos == newPos)
                        return;

                    td.LocalPosition = td.Parent == null ? newPos : td.Parent.InverseTransformPoint(newPos);
                    prop.FindPropertyRelative("LocalPosition").vector3Value = td.LocalPosition;
                    sObj.ApplyModifiedProperties();
                    break;
                }

                case Tool.Rotate:
                {
                    var newRot = Handles.RotationHandle(rot, pos);
                    if (rot == newRot)
                        return;

                    td.LocalRotation = td.Parent == null
                        ? newRot.eulerAngles
                        : (Quaternion.Inverse(td.Parent.rotation) * newRot).eulerAngles;
                    prop.FindPropertyRelative("LocalRotation").vector3Value = td.LocalRotation;
                    sObj.ApplyModifiedProperties();
                    break;
                }

                case Tool.View:
                case Tool.Scale:
                case Tool.Rect:
                case Tool.Transform:
                case Tool.Custom:
                case Tool.None: break;
                default: throw new ArgumentOutOfRangeException();
            }
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var parentProp = property.FindPropertyRelative(nameof(TransformData.Parent));
            var localPosProp = property.FindPropertyRelative(nameof(TransformData.LocalPosition));
            var localRotProp = property.FindPropertyRelative(nameof(TransformData.LocalRotation));

            var rowA = new Rect(position.x, position.y, position.width, 16f);
            var rowB = new Rect(position.x, position.y + 18f, position.width, 16f);
            var rowC = new Rect(position.x, position.y + 36f, position.width, 16f);

            var labelButton = new Rect(rowA.x, rowA.y, 100f, rowA.height);
            var restRowA = new Rect(rowA.x+labelButton.width, rowA.y, rowA.width-labelButton.width, 16f);
            //Debug.Log(m_lastSelectedPropertyPath == property);
            var sObj = m_selection.LastSerializedObject;
            var propPath = m_selection.LastSelectedPropertyPath;

            if (sObj == property.serializedObject && !k_passivePaths.Contains(property.propertyPath))
                k_passivePaths.Add(property.propertyPath);

            var selected = string.Equals(propPath, property.propertyPath) &&
                           sObj == property.serializedObject;
            if (GUI.Toggle(labelButton, selected, "Transform", "Button"))
                Select(property);
            else if(selected) 
                Deselect();

            EditorGUI.PropertyField(restRowA, parentProp, new GUIContent(""));
            EditorGUI.PropertyField(rowB, localPosProp, new GUIContent("Loc-Pos"));
            EditorGUI.PropertyField(rowC, localRotProp, new GUIContent("Loc-Rot"));
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) => base.GetPropertyHeight(property, label) * 3;
    }
#endif
}
