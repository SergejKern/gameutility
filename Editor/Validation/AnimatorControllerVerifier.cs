﻿using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using GameUtility.Validation;
using UnityEditor;
using UnityEngine;
using UnityEditor.Animations;
using _Editor.CustomAssetData;
using Core.Unity.Interface;
using JetBrains.Annotations;

namespace _Editor.Validation
{
    // referenced by reflection
    [UsedImplicitly]
    public class AnimatorControllerVerifier : IOnSaveVerifier
    {
        public void VerifyScene(ref VerificationResult res)
        {
            var animators = Object.FindObjectsOfType<Animator>();
            var controllersToVerify = new List<AnimatorController>();

            foreach (var anim in animators)
            {
                var controller = anim.runtimeAnimatorController as AnimatorController;
                if (controller == null)
                    continue;
                if (controllersToVerify.Contains(controller))
                    continue;
                controllersToVerify.Add(controller);
            }

            VerifyAnimators(controllersToVerify, ref res);

            // ReSharper disable SuspiciousTypeConversion.Global
            var animatorValidators = Object.FindObjectsOfType<MonoBehaviour>().OfType<IAnimatorStateValidation>();
            VerifyAnimatorStatesExist(ref res, animatorValidators, "Scene");

            var animatorCollectionStateValidations = Object.FindObjectsOfType<MonoBehaviour>().OfType<IAnimatorCollectionStateValidation>();
            VerifyAnimatorStatesExist(ref res, animatorCollectionStateValidations, "Scene");
            // ReSharper restore SuspiciousTypeConversion.Global

            PlayMakerVerification(ref res);
        }

        static void PlayMakerVerification(ref VerificationResult result)
        {
#if PLAYMAKER
            var playMakerScripts = Object.FindObjectsOfType<PlayMakerFSM>();
            foreach (var script in playMakerScripts)
            {
                foreach (var state in script.FsmStates)
                {
                    // todo 1: how to check if not initialized?
                    if (!state.IsInitialized)
                    {
                        Debug.LogWarning($"Cannot verify state {state.Name}, because it is not initialized!");
                        continue;
                    }
                    // ReSharper disable SuspiciousTypeConversion.Global
                    var animatorFSM = state.Actions.OfType<IAnimatorStateValidation>();
                    VerifyAnimatorStatesExist(ref result, animatorFSM, $"PlayMakerFSM {script} > State {state}");


                    var animatorCollectionFSM = state.Actions.OfType<IAnimatorCollectionStateValidation>();
                    VerifyAnimatorStatesExist(ref result, animatorCollectionFSM, $"PlayMakerFSM {script} > State {state}");
                    // ReSharper restore SuspiciousTypeConversion.Global

                }
            }
#endif
        }

        static void VerifyAnimatorStatesExist(ref VerificationResult result,
            IEnumerable<IAnimatorCollectionStateValidation> animatorFSM, string location)
        {
            foreach (var validator in animatorFSM)
                VerifyAnimatorStatesExist(ref result, validator, location);
        }

        static void VerifyAnimatorStatesExist(ref VerificationResult result, 
            IAnimatorCollectionStateValidation validator, string location)
        {
            foreach (var val in validator.Data)
                VerifyAnimatorStatesExist(ref result, val, location);
        }

        static void VerifyAnimatorStatesExist(ref VerificationResult result, 
            IEnumerable<IAnimatorStateValidation> animatorFSM, string location)
        {
            foreach (var validator in animatorFSM)
                VerifyAnimatorStatesExist(ref result, validator, location);
        }

        static void VerifyAnimatorStatesExist(ref VerificationResult result, 
            IAnimatorStateValidation validator,
            string location)
        {
            if (validator.AnimatorController == null)
            {
                result.Error($"No AnimatorController to validate in {location} > {validator}!", null); // (Object) validator);
                return;
            }

            if (VerifyStatesExist(validator.AnimatorController as AnimatorController, validator.States, out var wrongStates))
                return;

            result.Error($"Animator state not found, assigned in {location} > " +
                         $"{validator}! States: {wrongStates.Stringify()}", null); // (Object) validator);
        }

        static bool VerifyStatesExist(AnimatorController controller, IEnumerable<string> states, out List<string> wrongStates)
        {
            if (controller == null)
            {
                Debug.LogError("Unexpected error!");
                wrongStates = null;
                return true;
            }

            var allStateNames = new List<string>();
            foreach (var layer in controller.layers)
            {
                var sm = layer.stateMachine;

                var names = sm.states.Select(s => s.state).Select(state => state.name).ToList();
                allStateNames.AddRange(names);
            }

            wrongStates = states.Where(s => !allStateNames.Contains(s)).ToList();
            return wrongStates.IsNullOrEmpty();
        }

        public void Verify(ref VerificationResult res, List<GameObject> prefabs)
        {
            var controllersToVerify = new List<AnimatorController>();

            VerificationResult result = VerificationResult.Default;

            foreach (var prefab in prefabs)
            {
                var animators = prefab.GetComponentsInChildren<Animator>();
                var stateVerifier = prefab.GetComponentsInChildren<IAnimatorStateValidation>();
                VerifyAnimatorStatesExist(ref result, stateVerifier, $"Prefab {prefab.name}");

                var stateVerifierCollection = prefab.GetComponentsInChildren<IAnimatorCollectionStateValidation>();
                VerifyAnimatorStatesExist(ref result, stateVerifierCollection, $"Prefab {prefab.name}");

                foreach (var anim in animators)
                {
                    var controller = anim.runtimeAnimatorController as AnimatorController;
                    if (controllersToVerify.Contains(controller))
                        continue;
                    controllersToVerify.Add(controller);
                }
            }

            VerifyAnimators(controllersToVerify, ref result);
        }

        public void Verify(ref VerificationResult res, List<ScriptableObject> configs)
        {
            foreach (var c in configs)
            {
                switch (c)
                {
                    case IAnimatorStateValidation stateVerifier:
                        VerifyAnimatorStatesExist(ref res, stateVerifier, $"Config {c.name}");
                        break;
                    case IAnimatorCollectionStateValidation stateVerifierCollection:
                        VerifyAnimatorStatesExist(ref res, stateVerifierCollection, $"Config {c.name}");
                        break;
                }
            }
        }

        static void VerifyAnimators(IEnumerable<AnimatorController> controllers, ref VerificationResult result)
        {
            foreach (var c in controllers)
            {
                VerifyAnimator(c, ref result);
            }
        }

        static void VerifyAnimator(AnimatorController controller, ref VerificationResult result)
        {
            AnimatorControllerCustomData.LoadCustomData(controller, out var customData);
            if (customData.IgnoreStateNameValidator)
                return;

            foreach (var l in controller.layers)
            {
                var sm = l.stateMachine;

                foreach (var s in sm.states)
                {
                    var state = s.state;

                    var clip = state.motion as AnimationClip;
                    if (clip == null)
                        continue;

                    if (string.Equals(clip.name, state.name))
                        continue;
                    result.Error($"AnimatorController {controller} state {state} has different name then clip {clip}!", controller);
                }
            }
        }

        [MenuItem("Tools/Verification/All AnimatorControllers")]
        public static void VerifyAllAnimatorControllers()
        {
            var controllers = Resources.FindObjectsOfTypeAll<AnimatorController>();
            VerificationResult result = default;
            VerifyAnimators(controllers, ref result);
        }

        public void Finish() {}
    }
}
